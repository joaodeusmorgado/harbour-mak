import QtQuick 2.0
import Sailfish.Silica 1.0


Item {
    id: backgrnd2

    Rectangle {
        id: clock
        anchors.centerIn: parent
        width: (parent.width > parent.height ? parent.height : parent.width) - Theme.paddingLarge
        height: width
        radius: width/2
        color: Theme.secondaryHighlightColor




        Rectangle {
            id: inner
            anchors.centerIn: parent
            width: parent.width * 0.9
            height: width
            radius: width/2
            color: Theme.highlightColor
            //opacity: Theme.highlightBackgroundOpacity



            property var dotObj

            Component.onCompleted: {

                var size = parent.width * 0.4
                var centerX = parent.width/2
                var centerY = parent.height/2
                //console.log("size: " + size + "centerX: "+centerX +"; centerY: "+centerY)


                for (var i=0;i < 2*Math.PI; i += (Math.PI/6) ) {
                    dotObj = createItem("ClockDot.qml", parent)
                    dotObj.width = inner.width * 0.05
                    dotObj.x = centerX + size * Math.cos(i) - dotObj.width/2
                    dotObj.y = centerY + size * Math.sin(i) - dotObj.height/2


                    //dotObj.x = Qt.binding( function(){ return centerX + size * Math.cos(i) } )
                    //dotObj.y = Qt.binding( function(){ return centerY + size * Math.sin(i) } )
                    //dotObj.width = Qt.binding( function(){ return inner.width * 0.05 } )
                    //console.log("dotX: "+dotObj.x +"; dotY: "+dotObj.y + "; width: "+ dotObj.width )
                }
            }



            PointersAndDisplay {
                width: page.width
                height: page.height
                anchors.centerIn: parent
                textColor: Theme.highlightBackgroundColor
                rectVisible: false
            }



        }



        /*Grid {
            anchors.fill: parent

            Rectangle {
                width: clock.width/4
                height: width
                color: Theme.primaryColor
            }
            Rectangle {
                width: clock.width/4
                height: width
                color: Theme.secondaryColor
            }
            Rectangle {
                width: clock.width/4
                height: width
                color: Theme.highlightColor
            }
            Rectangle {
                width: clock.width/4
                height: width
                color: Theme.secondaryHighlightColor
            }
            Rectangle {
                width: clock.width/4
                height: width
                color: Theme.highlightBackgroundColor
                opacity: Theme.highlightBackgroundOpacity
            }
            Rectangle {
                width: clock.width/4
                height: width
                color: Theme.highlightDimmerColor
            }
        }*/

    }


    function createItem(itemName, parentName) {
        var component = Qt.createComponent(itemName);
        //finish creation
        if (component.status === Component.Ready) {
            var myItem = component.createObject(parentName);
            if (myItem === null) {// Error handling
                console.log("Error creating item")
            }
            else {
                //console.log("Ok creating")
                return myItem
            }
        }
        else if (component.status === Component.Error) {
            //Error handling
            console.log("Error handling component:",component.errorString());
        }
    }


}
