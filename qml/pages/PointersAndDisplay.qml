import QtQuick 2.0
import Sailfish.Silica 1.0
import QtMultimedia 5.0
import "../js/countdown.js" as Conv

Item {
    id: mainItem

    property alias textColor: timeSeparator.color
    property alias textBold: timeSeparator.font.bold
    property alias rectVisible: timeLCD.visible


    Item {
         id: clock
         anchors.centerIn: parent
         width: parent.width > parent.height ? parent.height : parent.width
         height: width

         MouseArea {
             id: clockClickArea
             anchors.fill: clock
             property bool bPositionChanged: false
             onPositionChanged: {
                 bPositionChanged = true
                 countDown.running = false;
                 secondPointer.visible = false;
                 var x_centro;
                 var y_centro;
                 var angulo;
                 var minutes;
                 var seconds;
                 var minutes_angle;
                 var seconds_angle;
                 x_centro = clock.width/2;
                 y_centro = clock.height/2;
                 angulo = Conv.getAngle(x_centro, y_centro, clockClickArea.mouseX, clockClickArea.mouseY);
                 minutes = Conv.getMinutesFromAngle(angulo);
                 seconds = Conv.getSecondsFromAngle(angulo);
                 minutes_angle = Conv.getMinutesAngleFromTime(minutes, seconds);
                 minuteRotation.angle = minutes_angle;
                 timeSeconds.text = ((seconds <= 9) ? ("0") : ("")) + seconds;
                 timeMinutes.text = ((minutes <= 9) ? ("0") : ("")) + minutes;
                 //mouse.accepted = true

             }
             onPressed: {
                 flick.interactive = false
             }

             onReleased: {
                 bPositionChanged = false
                 if (minuteRotation.angle)
                     countDown.running = true

                 flick.interactive = true
             }
             onClicked: {
                 if (alarm.playing) {
                     countDown.running = false
                     alarm.stop()
                 }
             }

             onPressAndHold: {
                 console.log("timer double tap")
                 if (!bPositionChanged)
                     timerReset()
             }
         }

    }


    //timer seconds pointer
    Image {
        id: secondPointer
        anchors.horizontalCenter: parent.horizontalCenter
        height: clock.width/2.86
        y: (centerPoint.y + centerPoint.height/2) - height
        source: "../images/second.png"
        smooth: true
        transform: Rotation {
            id: secondRotation
            origin.x: 2.5
            origin.y: secondPointer.height
        }
    }

    //timer minutes pointer
    Image {
        id: minutePointer
        anchors.horizontalCenter: parent.horizontalCenter
        height: secondPointer.height
        y: (centerPoint.y + centerPoint.height/2) - height
        source: "../images/minute.png"
        smooth: true
        transform: Rotation {
            id: minuteRotation
            origin.x: 6.5
            origin.y: minutePointer.height
        }
    }


    // timer dot center
    Image {
        id: centerPoint
        anchors.centerIn: clock
        width: 3*mm
        height: 3*mm
        sourceSize.width: centerPoint.width
        sourceSize.height: centerPoint.height
        fillMode: Image.PreserveAspectFit
        source: "../images/center.png"
    }

    ////////////////////////////////////////////////////

    // white square time place holder
    Item {
        id: recPlaceHolder
        width: timeSeparator.width + timeMinutes.width + timeSeconds.width + Theme.paddingSmall*2
        height: timeSeparator.height + Theme.paddingSmall*2
        anchors {
            horizontalCenter: parent.horizontalCenter
            top: centerPoint.bottom
            topMargin: Theme.paddingMedium
        }


        Rectangle {
            id: timeLCD
            width: timeSeparator.width + timeMinutes.width + timeSeconds.width + Theme.paddingSmall*2
            height: timeSeparator.height + Theme.paddingSmall*2
            radius: 8
            color: "#ffffff"
            anchors.centerIn: parent
        }


        Text {
            id: timeSeparator
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: ":"
            color: "black"
            font.family: Theme.fontFamily
            font.pixelSize: Theme.fontSizeMedium
            font.bold: true
        }

        Text {
            id: timeMinutes
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: timeSeparator.left
            text: "00"
            color: timeSeparator.color
            font.family: Theme.fontFamily
            font.pixelSize: Theme.fontSizeMedium
            font.bold: timeSeparator.font.bold
        }

        Text {
            id: timeSeconds
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: timeSeparator.right
            text: "00"
            color: timeSeparator.color
            font.family: Theme.fontFamily
            font.pixelSize: Theme.fontSizeMedium
            font.bold: timeSeparator.font.bold
        }


    }

    ////////////////////////////

    Timer {
        id: countDown
        interval: 1000
        repeat: true
        running: false
        onTriggered: {
            secondPointer.visible = true;
            var minutes;
            var seconds;
            var minutes_angle;
            var seconds_angle;
            var total_seconds;
            minutes = parseInt(timeMinutes.text, 10);
            seconds = parseInt(timeSeconds.text, 10);
            total_seconds = Conv.convertMinSecToSec(minutes, seconds);
            total_seconds--;
            if (total_seconds <= 0) {
                minutes = seconds = total_seconds = 0;
                /*minuteRotation.angle = minutes_angle;
                secondRotation.angle = seconds_angle;
                timeSeconds.text = "00"
                timeMinutes.text = "00"*/

                countDown.running = false;
                alarm.play();
            }
            minutes = Conv.convertSecToMinSec(total_seconds, 1);
            seconds = Conv.convertSecToMinSec(total_seconds, 2);
            minutes_angle = Conv.getMinutesAngleFromTime(minutes, seconds);
            seconds_angle = Conv.getSecondsAngleFromTime(seconds);
            minuteRotation.angle = minutes_angle;
            secondRotation.angle = seconds_angle;
            timeSeconds.text = ((seconds <= 9) ? ("0") : ("")) + seconds;
            timeMinutes.text = ((minutes <= 9) ? ("0") : ("")) + minutes;
        }
    }

    SoundEffect {
        id: alarm
        source: "../sounds/beep-01.wav"
        loops: 50//SoundEffect.Infinite
    }

    function timerReset() {
        countDown.running = false
        timeMinutes.text = "00"
        timeSeconds.text = "00"
        secondRotation.angle = 0
        minuteRotation.angle = 0
        alarm.stop()
    }
}
