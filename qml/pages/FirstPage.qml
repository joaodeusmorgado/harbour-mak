import QtQuick 2.0
import QtQuick.Window 2.1
import Sailfish.Silica 1.0
import QtMultimedia 5.0
import "../js/countdown.js" as Conv

Page {
    id: page

    property real mm: Screen.pixelDensity

    // To enable PullDownMenu, place our content in a SilicaFlickable
   SilicaFlickable {
       id: flick
        anchors.fill: parent


        // PullDownMenu and PushUpMenu must be declared in SilicaFlickable, SilicaListView or SilicaGridView
        PullDownMenu {
            MenuItem {
                text: qsTr("About")
                onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
            }
            MenuItem {
                text: qsTr("Change timer")
                onClicked: mainItem.visible = !mainItem.visible
            }
        }


        // Tell SilicaFlickable the height of its content.
        contentHeight: mainItem.height


        Background1 {
            id: mainItem
            width: page.width //>= flick.height ? flick.height : flick.width
            height: page.height
            visible: false
        }

        Background2 {
            id: backgroundTheme
            width: page.width
            height: page.height
            visible: !mainItem.visible
        }


        /*Item {
            id: mainItem
            width: page.width //>= flick.height ? flick.height : flick.width
            height: page.height

            Image {
                 id: clock
                 anchors.centerIn: parent
                 width: parent.width > parent.height ? parent.height : parent.width
                 height: width
                 source: "../images/clock.png"
                 //sourceSize.width: clock.width
                 //sourceSize.height: clock.height
                 fillMode: Image.PreserveAspectFit

                 PointersAndDisplay {
                     width: page.width //>= flick.height ? flick.height : flick.width
                     height: page.height
                     anchors.centerIn: parent
                 }
            }
        }*/

        /*Item {
            id: mainItem
            width: page.width //>= flick.height ? flick.height : flick.width
            height: page.height


            Image {
                 id: clock
                 anchors.centerIn: parent
                 width: parent.width > parent.height ? parent.height : parent.width
                 height: width
                 source: "../images/clock.png"
                 //sourceSize.width: clock.width
                 //sourceSize.height: clock.height
                 fillMode: Image.PreserveAspectFit


                 MouseArea {
                     id: clockClickArea
                     anchors.fill: clock
                     onPositionChanged: {
                         countDown.running = false;
                         secondPointer.visible = false;
                         var x_centro;
                         var y_centro;
                         var angulo;
                         var minutes;
                         var seconds;
                         var minutes_angle;
                         var seconds_angle;
                         x_centro = clock.width/2;
                         y_centro = clock.height/2;
                         angulo = Conv.getAngle(x_centro, y_centro, clockClickArea.mouseX, clockClickArea.mouseY);
                         minutes = Conv.getMinutesFromAngle(angulo);
                         seconds = Conv.getSecondsFromAngle(angulo);
                         minutes_angle = Conv.getMinutesAngleFromTime(minutes, seconds);
                         minuteRotation.angle = minutes_angle;
                         timeSeconds.text = ((seconds <= 9) ? ("0") : ("")) + seconds;
                         timeMinutes.text = ((minutes <= 9) ? ("0") : ("")) + minutes;
                         mouse.accepted = true
                     }
                     onPressed: {
                         flick.interactive = false
                     }

                     onReleased: {
                         countDown.running = true
                         flick.interactive = true
                     }
                     onClicked: {
                         if (alarm.playing) {
                             countDown.running = false
                             alarm.stop()
                         }
                     }
                 }

            }


            Image {
                id: secondPointer
                anchors.horizontalCenter: parent.horizontalCenter
                height: clock.width/2.86
                y: (centerPoint.y + centerPoint.height/2) - height
                source: "../images/second.png"
                smooth: true
                transform: Rotation {
                    id: secondRotation
                    origin.x: 2.5
                    origin.y: secondPointer.height
                }
            }

            Image {
                id: minutePointer
                anchors.horizontalCenter: parent.horizontalCenter
                height: secondPointer.height
                y: (centerPoint.y + centerPoint.height/2) - height
                source: "../images/minute.png"
                smooth: true
                transform: Rotation {
                    id: minuteRotation
                    origin.x: 6.5
                    origin.y: minutePointer.height
                }
            }


            Image {
                id: centerPoint
                anchors.centerIn: clock
                width: 3*mm
                height: 3*mm
                sourceSize.width: centerPoint.width
                sourceSize.height: centerPoint.height
                fillMode: Image.PreserveAspectFit
                source: "../images/center.png"
            }

            Rectangle {
                id: timeLCD
                width: timeSeparator.width + timeMinutes.width + timeSeconds.width + Theme.paddingSmall*2
                height: timeSeparator.height + Theme.paddingSmall*2
                radius: 8
                color: "#ffffff"
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    top: centerPoint.bottom
                    topMargin: Theme.paddingMedium
                }


                Text {
                    id: timeSeparator
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    text: ":"
                    color: "#000000"
                    font.family: Theme.fontFamily
                    font.pixelSize: Theme.fontSizeMedium
                }

                Text {
                    id: timeMinutes
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right: timeSeparator.left
                    text: "00"
                    color: "#000000"
                    font.family: Theme.fontFamily
                    font.pixelSize: Theme.fontSizeMedium
                }

                Text {
                    id: timeSeconds
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: timeSeparator.right
                    text: "00"
                    color: "#000000"
                    font.family: Theme.fontFamily
                    font.pixelSize: Theme.fontSizeMedium
                }
            }

            Timer {
                id: countDown
                interval: 1000
                repeat: true
                running: false
                onTriggered: {
                    secondPointer.visible = true;
                    var minutes;
                    var seconds;
                    var minutes_angle;
                    var seconds_angle;
                    var total_seconds;
                    minutes = parseInt(timeMinutes.text, 10);
                    seconds = parseInt(timeSeconds.text, 10);
                    total_seconds = Conv.convertMinSecToSec(minutes, seconds);
                    total_seconds--;
                    if (total_seconds <= 0) {
                        minutes = seconds = total_seconds = 0;

                        countDown.running = false;
                        alarm.play();
                    }
                    minutes = Conv.convertSecToMinSec(total_seconds, 1);
                    seconds = Conv.convertSecToMinSec(total_seconds, 2);
                    minutes_angle = Conv.getMinutesAngleFromTime(minutes, seconds);
                    seconds_angle = Conv.getSecondsAngleFromTime(seconds);
                    minuteRotation.angle = minutes_angle;
                    secondRotation.angle = seconds_angle;
                    timeSeconds.text = ((seconds <= 9) ? ("0") : ("")) + seconds;
                    timeMinutes.text = ((minutes <= 9) ? ("0") : ("")) + minutes;
                }
            }

            SoundEffect {
                id: alarm
                source: "../sounds/beep-01.wav"
                loops: 50//SoundEffect.Infinite
            }

        }*/



    }//SilicaFlickable
}
