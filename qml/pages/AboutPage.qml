import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page
    SilicaFlickable {
        id: listView
        anchors.fill: parent

        contentHeight: tex1.height + tex2.height + Theme.paddingMedium*2


        Text {
            id: tex1
            //anchors.top: parent.top
            anchors.left: parent.left
            //anchors.verticalCenter: parent.verticalCenter
            //width: parent.width
            anchors.margins: Theme.paddingMedium
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            font.family: Theme.fontFamily
            font.pixelSize: Theme.fontSizeSmall
            color: Theme.highlightColor
            textFormat: Text.RichText
            text: "<style>a:link { color: " + Theme.highlightColor + "; }</style><br>" +
                qsTr("<b>MAK - Mom Alarm Kitchen</b><br>Minimalistic kitchen timer<br><br>"+
                     "<b>Lead Designer and Sailfish port</b><br>"+
                     "João de Deus<br><br>"+
                     "<b>Original Meego Developers</b><br>Flávio Simões and João de Deus<br><br>"+

                     "<a href=\"https://bitbucket.org/joaodeusmorgado/harbour-mak\">"+
                     "<b>Souce code</b></a>")
            onLinkActivated: Qt.openUrlExternally("https://bitbucket.org/joaodeusmorgado/harbour-mak")
        }

        Text {
            id: tex2
            anchors.top: tex1.bottom
            //anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            //width: parent.width
            anchors.margins: Theme.paddingMedium
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            font.family: Theme.fontFamily
            font.pixelSize: Theme.fontSizeSmall
            color: Theme.highlightColor
            textFormat: Text.RichText
            text: "<style>a:link { color: " + Theme.highlightColor + "; }</style><br>" +
                qsTr("<b>Website:</b> <a href=\"http://soft-ingenium.planetaclix.pt\"> soft-ingenium.planetaclix.pt</a><br>"+
                     "<b>E-mail:</b> joaodeusmorgado@yahoo.com<br>"+
                     "<b>Version:</b> 0.2<br><br>"+
                     "<b>Tip:</b> Press and hold to reset a running timer.")
            onLinkActivated: Qt.openUrlExternally("http://soft-ingenium.planetaclix.pt")
        }

        VerticalScrollDecorator {}
    }
}
